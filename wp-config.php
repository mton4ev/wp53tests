<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress53' );

/** MySQL database username */
define( 'DB_USER', 'wordpress53' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress53dbpass' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );



define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&vwr==u[YFP?J+@a0>xC]p`2j&(=TvGzHB0QGIt+zQ;2A7OA6Lc`|vpq_x*9!2/S' );
define( 'SECURE_AUTH_KEY',  'Az7mrCC*D>mE@c:GXg^Sb6&@`pB`t(!.?lke@?J!U!na-/7`Qr [3Ds}yXY.}*j9' );
define( 'LOGGED_IN_KEY',    '~ruZ>7{FG3(-.YeD2n|A7::p^pfNa2^A<((HuIF`kc&54TF:::vP%v%R:E)Sm&z0' );
define( 'NONCE_KEY',        'q/ts^WI,usE&L+Y^bp8im|{sNljUMu>lE{N$XKzw}Q<>4YVG=q%]6_k&&JbF]nR`' );
define( 'AUTH_SALT',        '^St%_%Iv`=k8Q>,C:Fp3_91c`sr.4a1jykPHCQ`j+wJZRQH(c%uk`7^H;D-0,)<D' );
define( 'SECURE_AUTH_SALT', ';=&_$~D0^x$?n]=F*g%r*mrrbdA(?HWO^oHA5hK?4!d*vW<OPBe$~[G}JiDfxema' );
define( 'LOGGED_IN_SALT',   'L)K2a_8w4j7n2E+%Sf[8CT$F6ay(HBF kwNJ _7Rsj$M1Ko~Qlfl3:t3.ry|enKa' );
define( 'NONCE_SALT',       'C>Rnm,k4K`~!#)@Z7{A>PEHs1#0*4((A6K1oj(*9cm/]`5P^Km xLoVhX*M;NZX4' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
